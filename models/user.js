const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
//create user schema
SALT_WORK_FACTOR = 10;
//create user schema

const  UserSchema = new Schema({
    username:{
        type:String,
        unique:true,
        required:[true,'username field is required']
    },
    GCMID:{
        type:String,
        required:[false,'GCMID field is required']
    },
    email:{
        type:String,
        unique:true,
        required:[true,'Email field is required']
    },
    password:{
        type:String,
        required:[true,'password is required']
    },
});
UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});



const User = mongoose.model('User',UserSchema);
module.exports=User;